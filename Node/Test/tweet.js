var express = require('express');
var server = express();
var tweets = [];
server.get("/",function(req,res){
	res.redirect('/index')
})
server.get('/index',function(req,res){
	res.render('index',{
		locals:{
			title:"Title",
			header:"Header",
			tweets:tweets
		}
	})
})
server.get("/tweets",function(req,res){
	res.send(tweets);
})
server.post('/send',express.bodyParser(),function(req,res){
	console.log(req.body)
	if(req.body && req.body.tweet){
		tweets.push(req.body.tweet)
		res.send({status:"ok",message:"Tweet received"})
	}else{
		res.send({status:"nok",message:"No Tweet received"})
	}
})
server.listen(8000);