var cluster = require("cluster");
var os = require("os");
var http = require("http");
if (cluster.isMaster) {
	for(var i=0;i<os.cpus().length;i++){
		var worker = cluster.fork();
		worker.on("message",function(msg){
			if(msg.memory){
				console.log("Worker "+msg.process+" use "+msg.memory.rss+" memory");
			}
		})		
	}
} else if (cluster.isWorker) {
	setInterval(function(){
		process.send({memory:process.memoryUsage(),process:process.pid})		
	},1000)
}