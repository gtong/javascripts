function onLoad(){
	initGl();
	initShader();
	initBuffer();
	gl.clearColor(0.0,0.0,0.0,1.0);
	gl.enable(DEPTH_TEST);
	draw()
}
function draw(){
	
}
function initBuffer(gl){
	var p3 = [
		1.0,1.0,0.0,
		1.0,0.0,0.0,
		0.0,1.0,0.0
	]
	var p4 = [
		1.0,0.0,0.0,
		1.0,1.0,0.0,
		0.0,1.0,0.0,
		0.0,0.0,0.0
	]
	var buffer3 = createBuffer();
	var buffer4 = createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,buffer3);
	gl.bufferData(gl.ARRAY_BUFFER,float32Array(buffer3),gl.STATIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER,buffer4);
	gl.bufferData(gl.ARRAY_BUFFER,float32Array(buffer4),gl.STATIC_DRAW);
	
}